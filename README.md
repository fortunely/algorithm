# algorithm

#### 介绍
使用Java编写的基础算法, 主要用于学习目的. 
使用JDK版本, JDK1.8

#### 软件架构
软件架构说明: src文件夹下包含了算法实现完整代码, 每个算法都在对应文件夹内xxxTest类中有测试代码.


#### 安装教程

1.  下载源码至本地, 如命令$git clone git@gitee.com:fortunely/algorithm.git
2.  创建java工程, 将src文件及其子文件完整拷贝到项目所在路径
3.  运行对应xxxTest类

#### 使用说明

1.  暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
