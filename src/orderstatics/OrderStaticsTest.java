package orderstatics;

import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.Random;

public class OrderStaticsTest {
    public static void main(String[] args) {
        int[] testData = new int[20];
        Random r = new Random();

        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(1000);
        }

        printData("test case for getMaxMin", testData);
        // get the max and the min value at the same time
        OrderStatics.getMaxMin(testData);


        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(1000);
        }
        printData("test case for selectMax", testData);
        // select max value
        int max = OrderStatics.selectMax(testData, 0, testData.length-1);
        System.out.println("max = " + max);

        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(1000);
        }
        printData("test case for randomizedSelect(k-th min)", testData);
        // select k-th min value
        int k = OrderStatics.randomizedSelect(testData, 0, testData.length-1, 2);
        System.out.println("k-th min = " + k);
    }

    public static void printData(String title, int[] data) {
        System.out.println(title);
        for (int i = 0; i < data.length; i++) {
            if(0 == i) System.out.print("[");
            else System.out.print(", ");
            System.out.print(data[i]);
        }
        System.out.println("]");
    }
}
