package sort;

import java.util.Random;

public class SortTest {
    public static void main(String[] args) {
        // generate test case using random data
        int[] testData = new int[50];
        Random r = new Random();
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(50);
        }
        // bubble sort
        BubbleSort.bubbleSort(testData, 0, testData.length-1);
        printData("bubble sort result: ", testData);


        // generate test case using random data
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(50);
        }
        // insert sort
        InsertSort.insertSort(testData);
        printData("insert sort result: ", testData);


        // generate test case using random data
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(50);
        }
        // quick sort
        QuickSort.quickSort(testData, 0, testData.length-1);
        printData("quick sort result: ", testData);

        // generate test case using random data
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(50);
        }
        // heap sort
        HeapSort.heapSort(testData);
        printData("heap sort result", testData);

        // generate test case using random data
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(50);
        }
        // merge sort
        MergeSort.mergeSort(testData, 0, testData.length-1);
        printData("heap sort result", testData);

        // generate test case using random data
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(50);
        }
        // array sort
        // using java API
        ArraysSort.arraysSort(testData, 0, testData.length-1);
        printData("arrays sort result", testData);

        // generate test case using random data
        for (int i = 0; i < testData.length; i++) {
            testData[i] = r.nextInt(20);
        }
        // counting sort
        CountingSort.countingSort(testData, 0, testData.length-1);
        printData("counting sort result", testData);

        // generate double test case using random data
        double[] doubleTestData = new double[100];
        for (int i = 0; i < doubleTestData.length; i++) {
            doubleTestData[i] = Math.random(); // range [0, 1)
        }
        // bucket sort
        BucketSort.bucketSort(doubleTestData);
        printData("bucket sort result", doubleTestData);

    }

    public static void printData(String title, double[] A) {
        System.out.println(title);
        System.out.println(A.length);
        System.out.print("[");
        for (int i = 0; i < A.length; i++) {
            if(i > 0) System.out.print(",");
            System.out.printf("%.4f", A[i]);
        }
        System.out.println("]");
    }

    public static void printData(String title, int[] A) {
        System.out.println(title);
        System.out.println(A.length);
        System.out.print("[");
        for (int i = 0; i < A.length; i++) {
            if(i > 0) System.out.print(",");
            System.out.print(A[i]);
        }
        System.out.println("]");
    }
}
