package sort;

public class BubbleSort {
    private static void exchange(int[] A, int d1, int d2) {
        int temp = A[d1];
        A[d1] = A[d2];
        A[d2] = temp;
    }

    public static void bubbleSort(int []A, int p, int r) {
        for (int i = 0; i < r-p; i++) { // r-p times sort
            // A[r-i+1] has been sorted, A[p..r-i] has not been sorted
            // get A[r-i] every sort time, namely max element in non-sorted list A, as the minimum element in sorted list
            for (int j = p+1; j <= r-i; j++) {
                if (A[j-1] > A[j])
                    exchange(A, j-1, j);
            }
        }
    }
}
